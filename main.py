import imp
import uuid
import time
from module.notify import notify
import numpy as np
import cv2 
import os


from utils.config import ALERT_WAIT_TIME
from module.detect_person import predict_movement
from module.upload import upload_image
from module.notify import notify

# env variables - important 
TEST_ENV = False
CAMERA_DEVICE_ID = 0
BASE_CAPTURE_WAIT_TIME = 10 # wait for 5 seconds before taking photo

person_detector_model = cv2.CascadeClassifier("models/fullbody_recognition_model.xml") # an opencv classifier
face_detector_model = cv2.CascadeClassifier("models/facial_recognition_model.xml") # an opencv classifier

#tmp dir to save images for uploading them to server
img_save_dir = os.getcwd() + "/tmp/" 

print('*'*30, "Loading Smart Security Surveillance", '*'*30)
print('[info] --> Initializing capture device')

# initialize cv2 camera 
cam = cv2.VideoCapture(CAMERA_DEVICE_ID)

if cam.read() == False:
    print('[info] --> Opening up the camera')
    cam.open()

# if camera is not working
is_camera_working = True
if not cam.isOpened():
    is_camera_working = False
    print('[Error] --> Can not open camera')

# wait for sometime before starting
print('*'*30, "Waiting for {} seconds before startup".format(BASE_CAPTURE_WAIT_TIME), '*'*30)
time.sleep(BASE_CAPTURE_WAIT_TIME) 


if not is_camera_working:
    print('[Error] --> Exiting camera not working error found')

# press q to stop process
while is_camera_working:
    print('\n')
    start_time = time.time()

    # read image from camera
    _, captured_frame = cam.read()
    
    print("[info] --> camera feed read tat - ", int(time.time()-start_time)*1000)

    if captured_frame is None:
        print("Error] -- failed to read valid feed from camera. Stopping ......exit.")
    
    # detect in frame movement
    print('[info] --> detecting movement in camera feed -- ')
    image_frame, person_movement_detected = predict_movement(captured_frame, captured_frame, person_detector_model)
    image_frame, face_movement_detected = predict_movement(captured_frame, image_frame, face_detector_model)
    cv2.imshow('camera feed', image_frame)

    if person_movement_detected or face_movement_detected:
        print('[info] --> movement detected in frame -- ')
        # only upload if not testing locally
        if not TEST_ENV:
            # cv2.imshow('person_face_bbox', person_face_bbox)
            print('[info] --> upload image to server started -- ')
            
            # if person_detected->True ; save image
            img_obj_name = str(uuid.uuid1()) + ".jpg"
            image_path = img_save_dir + img_obj_name
            cv2.imwrite(image_path, captured_frame)

            # Upload image to server
            upload_flag, image_url = upload_image(image_path)

            if upload_flag:
                print('[info] --> sending alert to consumer -- ')
                success_flag = notify(image_url)
                # if failed to notify, just retry again and dont sleep
                if not success_flag:
                    continue
                # if notified then wait for sometime before re-starting
                else:
                    # Sleep for a ALERT_WAIT_TIME then restart
                    print('[info] --> Waiting before re-starting ..................')
                    time.sleep(ALERT_WAIT_TIME)
                    
                
    print("[info] --> camera feed frame process success tat - ", int(time.time()-start_time)*1000)
    # uncomment when imshow window is opened up
    if cv2.waitKey(1)&0xFF == ord('q'):
        break

cam.release()
cv2.destroyAllWindows()

