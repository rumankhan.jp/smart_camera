import cv2
import numpy as np



def predict_movement(image_frame, base_image, object_classifier):
    found_objects = False
    gray = cv2.cvtColor(image_frame, cv2.COLOR_BGR2GRAY)

    objects = object_classifier.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    if len(objects) > 0:
        found_objects = True

    # Draw a rectangle around the objects
    for (x, y, w, h) in objects:
        base_image = cv2.rectangle(base_image, (x, y), (x + w, y + h), (0, 255, 0), 2)

    return base_image, found_objects