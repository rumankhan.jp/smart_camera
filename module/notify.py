from mailjet_rest import Client
import os
import time
from datetime import datetime
from utils.config import MAILJET_API_KEY, MAILJET_SECRET_KEY, RECEIVER_EMAIL, RECEIVER_NAME

email_template1 = """\
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<h4>Movement detected at your property</h4>
    """
email_template2 = """\
</body>
</html>
"""


def notify(image_url):
    start_time = time.time()
    # get email subject
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S") # dd/mm/YY H:M:S
    mail_subject = "[smart_security] new alert at " + dt_string

    # prepare email HTML body
    time_html_format = "<p>at time {}</p>".format(dt_string)
    url_html_format = "<img src='{}' />".format(image_url)
    email_template = email_template1 + time_html_format + url_html_format + email_template2

    
    mailjet = Client(auth=(MAILJET_API_KEY, MAILJET_SECRET_KEY), version='v3.1')
    data = {
    'Messages': [
        {
        "From": {
            "Email": "rumankhan.jp@gmail.com",
            "Name": "Notify bot"
        },
        "To": [
            {
            "Email": RECEIVER_EMAIL,
            "Name": RECEIVER_NAME
            }
        ],
        "Subject": mail_subject,
        "HTMLPart": email_template,
        "CustomID": "AppGettingStartedTest"
        }
    ]
    }

    is_success = True
    result = mailjet.send.create(data=data)
    resp_status_code = result.status_code
    if resp_status_code == 200:
        print("[info] --> Successfully notified {} to {} - tat - {} ".format(RECEIVER_NAME,RECEIVER_EMAIL,int(time.time()-start_time)*1000))
    else:
        is_success = False
        print("mailjet server response --- ", result.json())
        print("[Error] --> Failed to notify {} - tat - {} ".format(RECEIVER_EMAIL,int(time.time()-start_time)*1000))

    return is_success