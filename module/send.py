import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from utils.config import SENDER_EMAIL, SENDER_PASSWORD
from datetime import datetime

email_template1 = """\
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<h4>Movement detected at your property</h4>
    """
email_template2 = """\
</body>
</html>
"""


def send_alert(business_mail, image_url):
    message = MIMEMultipart("alternative")
    # datetime object containing current date and time
    now = datetime.now()
    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

    message["Subject"] = "[smart_security] new alert at" + dt_string
    message["From"] = SENDER_EMAIL
    message["To"] = business_mail
  
    time_html_format = "<p>at time {}</p>".format(dt_string)
    url_html_format = "<img src='{}' />".format(image_url)
    # add this table_string to the email_template
    email_template = email_template1 + time_html_format + url_html_format + email_template2
    # Turn these into plain/html MIMEText objects
    # part1 = MIMEText(text, "plain")
    part2 = MIMEText(email_template, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    # message.attach(part1)
    message.attach(part2)

    # Create secure connection with server and send email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(SENDER_EMAIL, SENDER_PASSWORD)
        server.sendmail(
            SENDER_EMAIL, business_mail, message.as_string()
        )