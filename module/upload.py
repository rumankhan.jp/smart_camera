import random
import base64
import os
import time
import traceback
from imagekitio import ImageKit
from utils.config import IMAGEKIT_PRIVATE_KEY, IMAGEKIT_PUBLIC_KEY, IMAGEKIT_URL_ENDPOINT


# init ImageKit
imagekit = ImageKit(
    private_key=IMAGEKIT_PRIVATE_KEY,
    public_key=IMAGEKIT_PUBLIC_KEY,
    url_endpoint=IMAGEKIT_URL_ENDPOINT
)

def upload_image(image_path):
    """
    image_path : absolute path of image
    return error_flag and image_url
    """
    start_time = time.time()
    upload_success_flag = True
    # Create base64 for the file
    with open(image_path, mode="rb") as img:
        imgstr = base64.b64encode(img.read())

    img_object_name = image_path.split('/')[-1]

    # upload to imagekit API
    try:
        upload = imagekit.upload(
            file=imgstr,
            file_name= img_object_name,
            options={
                "response_fields": ["is_private_file", "folder"],
                "folder" : 'cctv',
            },
        )
        os.remove(image_path)
        
    except:
        os.remove(image_path)
        print("[Error] --> failed to upload to server. Something went wrong.")
        upload_success_flag = False
        return upload_success_flag, None

     # 6. Check the Imagekit API response 
    if upload.get("error") is not None:
        # add alert 
        upload_success_flag = False
        return upload_success_flag, None

    # get image url from response
    img_url = upload.get('response').get('url')
    # now save all fields to model

    if upload_success_flag:
        print("[info] --> uploaded successfully to ImageKit server. Image url - ",img_url)

    return upload_success_flag, img_url